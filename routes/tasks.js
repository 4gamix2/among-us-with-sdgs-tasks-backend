import express from "express";


import { getOnce,addOnce, getAll, getListByUser, updateOnce } from "../controllers/tasks.js";

const router = express.Router();
/*
router
  .route("/")
  .get(getAll)
  .post(
    multer("image", 5 * 1024 * 1024),
    body("title").isLength({ min: 5 }),
    body("description").isLength({ min: 5 }),
    body("price").isNumeric(),
    body("quantity").isNumeric(),
    addOnce
  );
*/
router
  .route("/:id")
  .get(getOnce);

  router
  .route("/byuser/:iduser")
  .get(getListByUser);

  router
  .route("/")
  .get(getAll)

  router
  .route("/update/:esm")
  .post( updateOnce);

  router
  .route("/add")
  .post( addOnce);

export default router;
