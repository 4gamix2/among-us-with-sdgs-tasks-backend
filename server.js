import express from 'express';
import mongoose from 'mongoose';
import morgan from 'morgan';
import cors from 'cors';

import { notFoundError, errorHandler } from './middlewares/error-handler.js';
import tasksRoutes from './routes/tasks.js';

const app = express();
const port = process.env.PORT || 9090;


mongoose.set('debug', true);
mongoose.Promise = global.Promise;

const atlasUri = 'mongodb+srv://jdeyiheb:0000@cluster0.ittl9oe.mongodb.net/aaa?retryWrites=true&w=majority&appName=Cluster0'; // Corrected URI with database name
mongoose.connect(atlasUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.on('connected', () => {
  console.log('Connected to MongoDB Atlas');
});

mongoose.connection.on('error', (err) => {
  console.error('MongoDB Atlas connection error:', err);
});

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/img', express.static('public/images'));

app.use('/tasks', tasksRoutes);

app.use(notFoundError);
app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});
