import { validationResult } from "express-validator";

import task from "../models/task.js";

/*export function getAll(req, res) {
  Game.find({})
    .then((docs) => {
      let list = [];
      for (let i = 0; i < docs.length; i++) {
        list.push({
          id: docs[i]._id,
          title: docs[i].title,
          price: docs[i].price,
        });
      }
      res.status(200).json(list);
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
}
*/



export async function addOnce(req, res) {
  const { task1, task2, task3, task4, iduser } = req.body;

  try {
      // Generate a unique string for esm
      const esmCode = await generateUniqueEsm();

      const newTask = await task.create({
          esm: esmCode,
          task1,
          task2,
          task3,
          task4,
          iduser,
      });

      res.status(200).json(esmCode);
  } catch (err) {
      console.error("Error adding task:", err);
      res.status(500).json({ error: err });
  }
}

// Function to generate a unique string for esm
async function generateUniqueEsm() {
  const prefix = "Tcode"; // Add your desired prefix here
  const randomNumber = Math.floor(100000 + Math.random() * 900000);
  const esmCode = `${prefix}${randomNumber}`;

  const exists = await task.exists({ esm: esmCode });
  if (exists) {
      // If the code already exists, recursively call the function to generate a new one
      return generateUniqueEsm();
  }
  return esmCode;
}


export function getOnce(req, res) {
  task.findById(req.params.id)
    .then((doc) => {
      res.status(200).json(doc);
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
}


export function getListByUser(req, res) {
  task.find({ iduser : req.params.iduser })
  .then((docs) => {
    let list = [];
    for (let i = 0; i < docs.length; i++) {
      list.push({
        id: docs[i]._id,
        esm : docs[i].esm,
        task1: docs[i].task1,
        task2: docs[i].task2,
        task3: docs[i].task3,
        task4: docs[i].task4,
        iduser: docs[i].iduser,
      });
    }
      res.status(200).json(list);
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
}


export function getAll(req, res) {
  task.find({})
    .then((docs) => {
      let list = [];
      for (let i = 0; i < docs.length; i++) {
        list.push({
          id: docs[i]._id,
          esm : docs[i].esm,
          task1: docs[i].task1,
          task2: docs[i].task2,
          task3: docs[i].task3,
          task4: docs[i].task4,
          iduser: docs[i].iduser,
        });
      }
      res.status(200).json(list);
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
}


export function updateOnce(req, res) {
  const { esm } = req.params; // Extract iduser from request parameters
  const updateData = req.body; // Assuming update data is sent in the request body

  task.findOneAndUpdate({ esm: esm }, updateData, { new: true })
      .then((updatedTask) => {
          if (updatedTask) {
              res.status(200).json(updatedTask); // Return the updated task
          } else {
              res.status(404).json({ error: "Task not found" });
          }
      })
      .catch((err) => {
          console.error("Error updating task:", err);
          res.status(500).json({ error: "Internal Server Error" });
      });
}


/*
export function putOnce(req, res) {
  let newGame = {};
  if(req.file == undefined) {
    newGame = {
      title: req.body.title,
      description: req.body.description,
      price: req.body.price,
      quantity: req.body.quantity
    }
  }
  else {
    newGame = {
      title: req.body.title,
      description: req.body.description,
      price: req.body.price,
      quantity: req.body.quantity,
      image: `${req.protocol}://${req.get("host")}/img/${req.file.filename}`
    }
  }
  Game.findByIdAndUpdate(req.params.id, newGame)
    .then((doc1) => {
      Game.findById(req.params.id)
        .then((doc2) => {
          res.status(200).json(doc2);
        })
        .catch((err) => {
          res.status(500).json({ error: err });
        });
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
}*/
