import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const tasksSchema = new Schema(
    {
        esm: {
            type: String,
        },task1: {
            type: String,
        
        },task2: {
            type: String,
        },task3: {
            type: String,
        },task4: {
            type: String,
        },iduser: {
            type: String,
        }
        
    },
    {
        timestamps: true
    }
);

export default model('tasks', tasksSchema);